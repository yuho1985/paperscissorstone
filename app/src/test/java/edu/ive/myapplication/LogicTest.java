package edu.ive.myapplication;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class LogicTest {
    @Test
    public void decideWinner_winTest(){
        //0 - paper, 1 - scissor, 2 - stone
        Logic gameLogic = new Logic();
        //result: -1 - player lose, 0 - draw, 1 - player win
        //test - player: paper, computer: stone
        int result = gameLogic.decideWinner(0, 2);
        assertEquals(1, result);
        //test - player: scissor, computer: paper
        result = gameLogic.decideWinner(1, 0);
        assertEquals(1, result);
        //test - player: stone, computer: scissor
        result = gameLogic.decideWinner(2, 1);
        assertEquals(1, result);
    }

    @Test
    public void decideWinner_drawTest(){
        //0 - paper, 1 - scissor, 2 - stone
        Logic gameLogic = new Logic();
        //result: -1 - player lose, 0 - draw, 1 - player win
        //test - player: paper, computer: paper
        int result = gameLogic.decideWinner(0, 0);
        assertEquals(0, result);
        //test - player: scissor, computer: scissor
        result = gameLogic.decideWinner(1, 1);
        assertEquals(0, result);
        //test - player: stone, computer: stone
        result = gameLogic.decideWinner(2, 2);
        assertEquals(0, result);
    }

    @Test
    public void decideWinner_loseTest(){
        //0 - paper, 1 - scissor, 2 - stone
        Logic gameLogic = new Logic();
        //result: -1 - player lose, 0 - draw, 1 - player win
        //test - player: paper, computer: scissor
        int result = gameLogic.decideWinner(0, 1);
        assertEquals(-1, result);
        //test - player: scissor, computer: stone
        result = gameLogic.decideWinner(1, 2);
        assertEquals(-1, result);
        //test - player: stone, computer: paper
        result = gameLogic.decideWinner(2, 0);
        assertEquals(-1, result);
    }
}
