package edu.ive.myapplication;

public class Logic {


    public int decideWinner(int playerInput, int computerInput) {
        if (playerInput == computerInput) {
            return 0;
        }
        if ((playerInput == 0 && computerInput == 2) ||
                (playerInput == 1 && computerInput == 0) ||
                (playerInput == 2 && computerInput == 1)) {
            return 1;
        }
        return -1;
    }
}
